import uuid

from flask import request, jsonify
from sqlalchemy.exc import IntegrityError
from werkzeug.security import generate_password_hash

from python_rest_service import app, db
from python_rest_service.bearer_authentication import token_required
from python_rest_service.models import User


@app.route('/users', methods=['GET'])
@token_required
def get_all_users(current_user):

    if not current_user.admin:
        return jsonify({'message': 'The user has no admin rights.'})

    users = User.query.all()
    response = []
    for user in users:
        user_data = {}
        user_data['public_id'] = user.public_id
        user_data['name'] = user.name
        user_data['password'] = user.password
        user_data['admin'] = user.admin
        response.append(user_data)
    return jsonify({'users': response})


@app.route('/user/<public_id>', methods=['GET'])
@token_required
def get_one_user(current_user, public_id):

    if not current_user.admin:
        return jsonify({'message': 'The user has no admin rights.'})

    user = User.query.filter_by(public_id=public_id).first()
    if not user:
        return jsonify({'message': 'No user found.'})

    user_data = {}
    user_data['public_id'] = user.public_id
    user_data['name'] = user.name
    user_data['password'] = user.password
    user_data['admin'] = user.admin
    return jsonify({'user': user_data})


@app.route('/user', methods=['POST'])
@token_required
def create_user(current_user):
    if not current_user.admin:
        return jsonify({'message': 'The user has no admin rights.'})

    data = request.get_json()
    hashed_password = generate_password_hash(data['password'], method='sha256')
    new_user = User(public_id=str(uuid.uuid4()),
                    name=data['name'],
                    password=hashed_password,
                    admin=False)
    db.session.add(new_user)
    try:
        db.session.commit()
        return jsonify({'message': 'User has been created.'}), 200
    except IntegrityError:
        return jsonify({'message': 'User with this username already exists.'}), 400


@app.route('/user/<public_id>', methods=['PUT'])
@token_required
def give_admin_rights(current_user, public_id):
    if not current_user.admin:
        return jsonify({'message': 'The user has no admin rights.'})

    user = User.query.filter_by(public_id=public_id).first()
    if not user:
        return jsonify({'message': 'No user found.'})
    if user.admin:
        return jsonify({'message': 'Already an admin.'})

    user.admin = True
    db.session.commit()
    return jsonify({'message': 'Admin rights have been added for this user.'})


@app.route('/user/<public_id>', methods=['DELETE'])
@token_required
def delete_user(current_user, public_id):
    if not current_user.admin:
        return jsonify({'message': 'The user has no admin rights.'})

    user = User.query.filter_by(public_id=public_id).first()
    if not user:
        return jsonify({'message': 'No user found.'})
    db.session.delete(user)
    db.session.commit()
    return jsonify({'message': 'The user has been deleted.'})
