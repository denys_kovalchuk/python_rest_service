import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SECRET_KEY'] = str(os.environ.get('SECRET_KEY'))
app.config['JWT_SECRET_KEY'] = str(os.environ.get('JWT_SECRET_KEY'))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///library.db'

db = SQLAlchemy(app)

from python_rest_service import routes, routes_user, routes_book
