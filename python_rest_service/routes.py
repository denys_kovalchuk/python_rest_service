import datetime

import jwt
from flask import request, jsonify
from werkzeug.security import check_password_hash

from python_rest_service import app
from python_rest_service.models import User


@app.route('/login')
def login():
    auth = request.authorization

    if not auth or not auth.username or not auth.password:
        return jsonify({'message': 'Missing username and/or password.'}), 401

    user = User.query.filter_by(name=auth.username).first()
    if not user:
        return jsonify({'message': 'Unknown username.'}), 401

    if check_password_hash(user.password, auth.password):
        token = jwt.encode({'public_id': user.public_id,
                            'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=60)},
                           app.config['JWT_SECRET_KEY'])
        return jsonify({'token': token.decode('UTF-8')})
    return jsonify({'message': 'Wrong credentials.'}), 401


@app.errorhandler(404)
def bad_request(error):
    response = {'message': 'Bad request.'}
    return jsonify(response), 400
