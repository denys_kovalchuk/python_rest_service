import uuid

from flask import request, jsonify
from sqlalchemy.exc import IntegrityError

from python_rest_service import app, db
from python_rest_service.bearer_authentication import token_required
from python_rest_service.models import Book


@app.route("/books", methods=['GET'])
@token_required
def get_all_books(current_user):
    books = Book.query.all()
    if not books:
        return jsonify({'message': 'Books have not been found.'}), 404

    response = {'books': []}
    for book in books:
        response['books'].append(str(book))
    return jsonify(response), 200


@app.route("/book/<public_id>", methods=['GET'])
@token_required
def get_book(current_user, public_id):
    book = Book.query.filter_by(public_id=public_id).first()

    if book:
        return book.json(), 200
    else:
        return jsonify({'message': 'Book has not been found.'}), 404


@app.route("/book", methods=['POST'])
@token_required
def add_book(current_user):
    if not current_user.admin:
        return jsonify({'message': 'The user has no admin rights.'})

    book = Book()
    book.ISBN = request.args.get('isbn')
    book.language = request.args.get('language')
    book.pages = request.args.get('pages')
    book.publisher = request.args.get('publisher')
    book.title = request.args.get('title')
    book.public_id = str(uuid.uuid4())
    db.session.add(book)
    try:
        db.session.commit()
        return jsonify({'message': 'Book has been added.'}), 200
    except IntegrityError:
        return jsonify({'message': 'Book with this ISBN already exists.'}), 400


@app.route("/book/<public_id>", methods=['PUT'])
@token_required
def modify_book(current_user, public_id):
    if not current_user.admin:
        return jsonify({'message': 'The user has no admin rights.'})

    book = Book.query.filter_by(public_id=public_id).first()
    if book:
        book.ISBN = request.args.get('isbn', book.ISBN)
        book.language = request.args.get('language', book.language)
        book.pages = request.args.get('pages', book.pages)
        book.publisher = request.args.get('publisher', book.publisher)
        book.title = request.args.get('title', book.title)
        try:
            db.session.commit()
            return jsonify({'message': 'Book has been updated.'}), 200
        except IntegrityError:
            return jsonify({'message': 'Book with this ISBN already exists.'}), 400
    return jsonify({'message': 'Book with ID has not been found.'}), 404


@app.route("/book/<public_id>", methods=['DELETE'])
@token_required
def remove_book(current_user, public_id):

    if not current_user.admin:
        return jsonify({'message': 'The user has no admin rights.'})

    book = Book.query.filter_by(public_id=public_id).first()
    if book:
        db.session.delete(book)
        db.session.commit()
    else:
        return jsonify({'message': 'Book with ID has not been found.'}), 404
    return jsonify({'message': 'The book has been deleted.'}), 200
