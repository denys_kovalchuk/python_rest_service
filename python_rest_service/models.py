from flask import jsonify

from python_rest_service import db


class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(50), unique=True)
    title = db.Column(db.String(50), unique=False, nullable=False)
    pages = db.Column(db.Integer, unique=False, nullable=True)
    publisher = db.Column(db.String(50), unique=False, nullable=True)
    language = db.Column(db.String(50), unique=False, nullable=True)
    ISBN = db.Column(db.String(50), unique=True, nullable=False)

    def json(self):
        return jsonify({'id': self.id,
                        'title': self.title,
                        'pages': self.pages,
                        'publisher': self.publisher,
                        'language': self.language,
                        'ISBN': self.ISBN,
                        'public_id': self.public_id}
                       )

    def __str__(self):
        return str({'id': self.id,
                    'title': self.title,
                    'pages': self.pages,
                    'publisher': self.publisher,
                    'language': self.language,
                    'ISBN': self.ISBN,
                    'public_id': self.public_id})


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(50), unique=True)
    name = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(80))
    admin = db.Column(db.Boolean)
