import getpass
import uuid

from werkzeug.security import generate_password_hash

from python_rest_service import db
from python_rest_service.models import User


def create_user():
    username = input('Username: ')
    user = User.query.filter_by(name=username).first()
    if user:
        return 'This username is already taken!'

    password = getpass.getpass('Password: ')

    hashed_password = generate_password_hash(password, method='sha256')
    new_user = User(public_id=str(uuid.uuid4()),
                    name=username,
                    password=hashed_password,
                    admin=True)
    db.session.add(new_user)
    db.session.commit()
    return 'User has been created.'


print('\nPlease enter credentials for new admin user: \n')
print(create_user())
