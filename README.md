# python_rest_service

1. Set environment variables:
    *   setx -m SECRET_KEY ""
    * 	setx -m JWT_SECRET_KEY ""

2. Create db
    * 	Go to run.py file location
    * 	Open python shell
    	```python
    	from python_rest_service import db
    	db.create_all()
    	exit()
3. Start the service with executing 'run.py' script
4. Add first admin user with 'create_admin_user.py' script.